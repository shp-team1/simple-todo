"""Схема(модель) реквеста/респонса"""
from fastapi import HTTPException
from pydantic import validator
from pydantic import BaseModel
from pydantic import EmailStr
from uuid import UUID

import re

ALLOWS_NAME_LETTER_PATTERN = re.compile(r"^[а-яА-Я+a-zA-Z+0-9+_]")


class TunedModel(BaseModel):
    class Config:
        "tells pydamic to convert any obj to json"
        orm_mode = True


class UserShow(TunedModel):
    user_id: UUID
    name: str
    email: EmailStr
    is_active: bool


class UserCreate(BaseModel):
    name: str
    email: EmailStr

    @validator("name")
    def validate_name(cls, value):
        if not ALLOWS_NAME_LETTER_PATTERN.match(value):
            raise(HTTPException(status_code=422, detail="Not allowed symbols in user name"))
        return value