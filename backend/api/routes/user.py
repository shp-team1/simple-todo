from fastapi.routing import APIRouter
from api.actions.user import _create
from api.shemas import UserCreate, UserShow


router = APIRouter()

@router.post("/", response_model=UserShow)
async def create(body: UserCreate) -> UserShow:
    return await _create(body)