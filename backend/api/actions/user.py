from api.shemas import  UserCreate, UserShow
from db import models
from db.dals import UserDAL
from db.sessions import async_session


async def _create(body: UserCreate) -> UserShow:
    async with async_session() as session:
        async with session.begin():
            user: models.User = await UserDAL(session).create(name=body.name, email=body.email)
            return UserShow(user_id=user.id, name=user.name, email=user.email, is_active=user.is_active)