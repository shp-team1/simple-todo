from envparse import Env

env = Env()

USER_NAME = env.str("POSTGRES_USER", default="postgres")
USER_PASSWORD = env.str("POSTGRES_PASSWORD", default="postgres")
BD_NAME = env.str("POSTGRES_DB", default="db")
PORT = "5432"
HOST = "0.0.0.0"


# REAL_DATABASE_URL = f"postgresql+asyncpg://{USER_NAME}:{USER_PASSWORD}@{HOST}:{PORT}/{BD_NAME}",

# MIGRATION_DATABASE_URL =  f"postgresql://{USER_NAME}:{USER_PASSWORD}@{HOST}:{PORT}/{BD_NAME}"

REAL_DATABASE_URL = "postgresql+asyncpg://postgres:postgres@0.0.0.0:5432/db"
MIGRATION_DATABASE_URL = "postgresql://postgres:postgres@0.0.0.0:5432/db"