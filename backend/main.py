from fastapi import FastAPI
import uvicorn
from api.routes import router as api_router
from fastapi.routing import APIRouter



app = FastAPI()

router = APIRouter()
router.include_router(api_router, prefix="/api", tags=["api"])

app.include_router(router)


if __name__ == "__main__":
    uvicorn.run(app, host="127.0.0.1", port=8000)