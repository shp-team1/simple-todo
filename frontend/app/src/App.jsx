import React from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import Login from  "./pages/login/index";
import Home from  "./pages/home/index";
 
const App = () => (
  <BrowserRouter>
      <Routes>
        <Route path='/' element={<Home />} />
        <Route path="login" element={<Login />} />
      </Routes>
    </BrowserRouter>
)

export default App;
