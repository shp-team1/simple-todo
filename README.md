# Проект todo
Стек технологий:
- python 3.9+
- react js
- fastAPI вместе с postgreSQL + Alembic и SQLalchemy на сервере Uvicorn (feature Gunicorn+Uvicorn workers)


## Для бекенда:
Немного о структуре проекта:
Главный файл - `main.py`
База данных - PostgreSQL

Модуль **db** отвечает за работу с базой данных:
Создаем модели(python) с помощью SQLalchemy (в файле `models.py`), создаем таблицы из моделей в реальной бд с помощью миграций от  Alembic. Для доступа к бд используем DAL-интерфейс(Data Access Layer), создается в `dals.py`.

Обработкой запросов занимается модуль **api**:
Валидацией, сериализацией/десериализацией данных занимсется библиотека Pydantic. Шаблон запроса/ответа (request/responce) создается в файле `shemas.py`.
Путь же реквеста указыватся в модуле *routes* в файле соответствующем названием модели, конечным результатом работы функции под декоратором маршрутизатора будет схема ответа (responce).

**_Начало работы_**
>`git clone https://gitlab.com/shp-team1/simple-todo.git`

>**Создаем виртуальное окружение для питона и импортируем библиотеки:**
`cd simple-todo/backend`
`python3 -m venv venv`
`source venv/bin/activate`
`pip install pip --upgrade`
`pip install -r "requirements.txt"`

>**Поднимаем постгрес бд в докер-контейнере:**
Созаем новый терминал и прописываем команды
`sudo apt install docker` 
`sudo apt install docker-compose`
`sudo docker pull postgres:latest` 
`sudo docker-compose -f docker-compose.yaml up`

> **Alembic миграции:**
Мы используем Alembic + SQLalchemy для обновления и взаимодействия с бд. 
При первом запуске проекта применяем миграции командой `alembic upgrade heads`
Далее при изменении моделей генерируем миграцию и применяем командами
`alembic revision --autogenerate -m "Коментарий"`
`alembic upgrade heads`


## Для фронтенда
пока ничего.
Удачи)